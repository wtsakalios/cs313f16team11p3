package edu.luc.etl.cs313.android.shapes.android;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import edu.luc.etl.cs313.android.shapes.model.*;

import static android.graphics.Paint.Style.FILL_AND_STROKE;

/**
 * A Visitor for drawing a shape to an Android canvas.
 */
public class Draw implements Visitor<Void> {

	// TODO entirely your job (except onCircle)

	private final Canvas canvas;

	private final Paint paint;

	public Draw(final Canvas canvas, final Paint paint) {
		this.canvas = canvas;
		this.paint = paint;
		paint.setStyle(Style.STROKE);
	}

	@Override
	public Void onCircle(final Circle c) {
		canvas.drawCircle(0, 0, c.getRadius(), paint);
		return null;
	}

	@Override
	public Void onStroke(final Stroke c) {
		int previous = paint.getColor();
		paint.setColor(c.getColor());
		c.getShape().accept(this);
		paint.setColor(previous);
		return null;
	}

	@Override
	public Void onFill(final Fill f) {
		Style previous = paint.getStyle();
		paint.setStyle(Style.FILL_AND_STROKE);
		f.getShape().accept(this);
		paint.setStyle(previous);
		return null;
	}

	@Override
	public Void onGroup(final Group g) {

		for (final Shape shape : g.getShapes()) {
			shape.accept(this);
		}

		return null;
	}

	@Override
	public Void onLocation(final Location l) {
		canvas.translate(l.getX(), l.getY());
		l.getShape().accept(this);
		canvas.translate(-l.getX(), -l.getY());

		return null;
	}

	@Override
	public Void onRectangle(final Rectangle r) {
		canvas.drawRect(0, 0, r.getWidth(), r.getHeight(), paint);

		return null;
	}

	@Override
	public Void onOutline(Outline o) {
		Style previous = paint.getStyle();
		paint.setStyle(Style.STROKE);
		o.getShape().accept(this);
		paint.setStyle(previous);

		return null;
	}

	@Override
	public Void onPolygon(final Polygon s) {
		int a = s.getPoints().size();
		float[] pts = null;
		pts = new float[4 * a];
		int b = 0;

		for(final Point point : s.getPoints()) {
			pts[b] = point.getX();
			pts[b+1] = point.getY();
			b += 4;
		}

		for (int c = 4; c < 4*a; c += 4){
			pts[c-2] = pts[c];
			pts[c-1] = pts[c+1];
		}

		pts[4*a-2] = pts[0];
		pts[4*a-1] = pts[1];

		canvas.drawLines(pts, paint);

		return null;
	}
}
