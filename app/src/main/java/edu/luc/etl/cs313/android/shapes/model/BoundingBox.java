package edu.luc.etl.cs313.android.shapes.model;

/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {

	// TODO entirely your job (except onCircle)

	@Override
	public Location onCircle(final Circle c) {
		final int radius = c.getRadius();

		return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));
	}

	@Override
	public Location onFill(final Fill f) {

		return f.getShape().accept(this);
	}

	@Override
	public Location onGroup(final Group g) {
		int up = Integer.MIN_VALUE;
		int down = Integer.MAX_VALUE;
		int left = Integer.MAX_VALUE;
		int right = Integer.MIN_VALUE;

		for(final Shape shape : g.getShapes()) {
			Location a = shape.accept(this);
			up = Math.max(up, a.getY() + ((Rectangle) a.getShape()).getHeight());
			down = Math.min(down, a.getY());
			left = Math.min(left, a.getX());
			right = Math.max(right, a.getX() + ((Rectangle) a.getShape()).getWidth());
		}

		return new Location(left, down, new Rectangle(right - left, up - down));
	}

	@Override
	public Location onLocation(final Location l) {
		Location a = l.getShape().accept(this);

		return new Location(l.getX() + a.getX(), l.getY() + a.getY(), a.getShape());
	}

	@Override
	public Location onRectangle(final Rectangle r) {

		return new Location(0,0,r);
	}

	@Override
	public Location onStroke(final Stroke c) {

		return c.getShape().accept(this);
	}

	@Override
	public Location onOutline(final Outline o) {

		return o.getShape().accept(this);
	}

	@Override
	public Location onPolygon(final Polygon s) {

		return onGroup(s);
	}
}
